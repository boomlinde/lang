class Parser():
    def __init__(self):
        self.stack = []
        self._words = {}

    def eval(self, string):
        pop = self.pop
        push = self.push
        parsed = self._parse(string)
        for token in parsed:
            if token[0] == "word":
                if token[1] == "python":
                    exec(self.pop())
                elif token[1] in self._words:
                    self.eval(self._words[token[1]])
                else:
                    try:
                        token[1] = int(token[1])
                    except:
                        try:
                            token[1] = float(token[1])
                        except:
                            pass
                    self.push(token[1])
            else:
                self.push(token[1])

    def push(self, o):
        self.stack.append(o)

    def pop(self):
        return self.stack.pop()

    def _parse(self, data):
        data = data.strip() + ' '
        whitespace = ' \t\n\r'
        depth = 0
        tokens = []
        item = ''
        ws = False
        escaped = False
        for c in data:
            if c == '\\' and not escaped:
                escaped = True
            elif c in whitespace and depth == 0:
                if not ws:
                    tokens.append([type, item])
                    item = ''
                ws = True
            else:
                ws = False
                if c == '[' and not escaped:
                    if depth != 0:
                        item += c
                    type = 'string'
                    depth += 1
                elif c == ']' and not escaped:
                    if depth != 1:
                        item += c
                    depth -= 1
                else:
                    if depth == 0:
                        type = 'word'
                    if escaped:
                        c = {'n': '\n', 't': '\t', 'r': '\r'}.get(c, c)
                    item += c
                escaped = False
        return tokens

if __name__ == "__main__":
    import sys
    parser = Parser()
    code = file("std", "r").read()
    if len(sys.argv) > 1:
        for arg in sys.argv[1:]:
            code += file(arg, "r").read() + "\n"
    parser.eval(code)
    while True:
        try:
            parser.eval(raw_input("ok\n"))
        except Exception, e:
            print e

